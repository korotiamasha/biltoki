jQuery(document).ready(function ($) {
    $('.select2').select2({
        minimumResultsForSearch: -1,
    });

    var mainSlider = new Swiper('.bi-main-banner__slider', {
        slidesPerView: 1,
        spaceBetween: 1,
        loop: true,
        pagination: {
            el: '.bi-main-banner-pagination',
            type: 'bullets',
            clickable: true,
        },
    });

    var trustSlider = new Swiper('.bi-trust__slider', {
        slidesPerView: 'auto',
        spaceBetween: 50,
        loop: true,
    });


    /*var aboutNav = new Swiper('.bi-about__nav', {
        slidesPerView: 'auto',
        spaceBetween: 36,
        centeredSlides: true,
        loop: false,

        breakpoints: {
            576: {
                centeredSlides: false,
                spaceBetween: 57,
            },
        }
    });*/


    var actualSlider = new Swiper('.bi-actual__actual', {
        slidesPerView: 'auto',
        spaceBetween: 14,
        loop: true,
        centeredSlides: true,
        pagination: {
            el: '.bi-actual-pagination',
            type: 'bullets',
            clickable: true,
        },
        breakpoints: {
            576: {
                spaceBetween: 30,
            },
        }
    });

    var placesSlider = new Swiper('.bi-places__slider', {
        slidesPerView: 'auto',
        spaceBetween: 10,
        loop: true,
        centeredSlides: true,
        breakpoints: {
            768: {
                spaceBetween: 30,
            },
        },
        pagination: {
            el: '.bi-places-pagination',
            type: 'bullets',
            clickable: true,
        },
    });

    var tradersSlider = new Swiper('.bi-traders__slider', {
        slidesPerView: 'auto',
        spaceBetween: 10,
        loop: true,
        centeredSlides: true,
        breakpoints: {
            768: {
                spaceBetween: 30,
            },
        },
        pagination: {
            el: '.bi-traders-pagination',
            type: 'bullets',
            clickable: true,
        },
    });

    var bigSlider = new Swiper('.bi-big-slider__swiper', {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: true,
        pagination: {
            el: '.bi-big-slider-pagination',
            type: 'bullets',
            clickable: true,
        },

        navigation: {
            nextEl: '.bi-big-slider-next',
            prevEl: '.bi-big-slider-prev',
        },
    });

    var reviewsSlider = new Swiper('.bi-reviews__slider', {
        slidesPerView: '1',
        spaceBetween: 30,
        loop: true,
        pagination: {
            el: '.bi-reviews-pagination',
            type: 'bullets',
            clickable: true,
        },

        navigation: {
            nextEl: '.bi-reviews-next',
            prevEl: '.bi-reviews-prev',
        },
    });

    var heartSlider = new Swiper('.bi-heart__slider', {
        slidesPerView: '1',
        spaceBetween: 30,
        loop: true,
        pagination: {
            el: '.bi-heart-pagination',
            type: 'bullets',
            clickable: true,
        },

        navigation: {
            nextEl: '.bi-heart-next',
            prevEl: '.bi-heart-prev',
        },
    });

    var spotlightSlider = new Swiper('.bi-spotlight__slider', {
        slidesPerView: '1',
        spaceBetween: 0,
        loop: true,
        pagination: {
            el: '.bi-spotlight-pagination',
            type: 'bullets',
            clickable: true,
        },
    });

    var adventureSlider = new Swiper('.bi-adventure-slider__swiper', {
        slidesPerView: 1,
        spaceBetween: 0,
        centeredSlides: true,
        loop: true,
        pagination: {
            el: '.bi-adventure-slider-pagination',
            type: 'bullets',
            clickable: true,
        },
        breakpoints: {
            576: {
                slidesPerView: 'auto',
            },
        }
    });


    var hallesSlider = new Swiper('.bi-halles-tags__slider', {
        slidesPerView: 'auto',
        spaceBetween: 42,
        centeredSlides: true,
        loop: false,
        breakpoints: {
            576: {
                spaceBetween: 57,
                centeredSlides: false,
            },
        }
    });


    var personSlider = new Swiper('.bi-person__slider', {
        slidesPerView: 'auto',
        spaceBetween: 10,
        centeredSlides: true,
        loop: false,
        breakpoints: {
            576: {
                spaceBetween: 28,
                centeredSlides: false,
            },
        },
        pagination: {
            el: '.bi-person-pagination',
            type: "progressbar",
            draggable: true,
        }, scrollbar: {
            el: '.bi-person-pagination',
            type: "progressbar",
            draggable: true,
            dragSize: "auto",
        },
    });


    var settings = {
        slidesPerView: 'auto',
        spaceBetween: 23,
        centeredSlides: true,
        loop: true,
        breakpoints: {
            576: {
                spaceBetween: 57,
            },
        }
    };
    planSlider = new Swiper('.bi-plan__slider', settings);

    /*
    window.addEventListener('load', function () {});

    window.addEventListener('resize', function () {});
    */

    $('.bi-news__tabs-item').on('click', function () {
        $(this).toggleClass('active');
    });

    /*header*/


    $(document).scroll(function () {
        let header = $('.bi-header');
        let navTop = header.position().top;
        var scroll = $(this).scrollTop();

        if (scroll > navTop) {
            header.addClass('fixed');
            $('.bi-header__logo-min, .bi-header__logo').addClass('black-logo');
        } else {
            header.removeClass('fixed');
            $('.bi-header__logo-min, .bi-header__logo').removeClass('black-logo');
        }
    });

    $('.bi-header__burger').on('click', function () {
        if ($('.bi-header').hasClass('open')) {
            $(this).removeClass('open');
            $('.bi-header').removeClass("open");
        } else {
            $(this).addClass('open');
            $('.bi-header').addClass("open");
        }
    })
});
